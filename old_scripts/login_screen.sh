#!/bin/bash
UP=`uptime | awk -F" up " '{print $2}'|awk -F" user" '{print $1}'|awk -F",  " '{print $1}'`
echo "$(tput setaf 2)

       .~~.   .~~.
      '. \ ' ' / .'   $(tput sgr0)  Welcome to Gonnella's $(tput setaf 1)
       .~ .~~~..~.    $(tput sgr0)                   _                           _ $(tput setaf 1)
      : .~.'~'.~. :   $(tput sgr0)   ___ ___ ___ ___| |_ ___ ___ ___ _ _     ___|_|$(tput setaf 1)
     ~ (   ) (   ) ~  $(tput sgr0)  |  _|__ |_ -| . | . | -_|  _|  _| | |   | . | |$(tput setaf 1)
    ( : '~'.~.'~' : ) $(tput sgr0)  |_| |___|___|  _|___|___|_| |_| |_  |   |  _|_|$(tput setaf 1)
     ~ .~ (   ) ~. ~  $(tput sgr0)              |_|                 |___|   |_|    $(tput setaf 1)
      (  : '~' :  )
       '~ .~~~. ~'   $(tput sgr0)    -- Uptime: $UP$(tput setaf 1)
           '~'
$(tput sgr0)"
