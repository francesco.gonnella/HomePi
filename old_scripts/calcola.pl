#! /usr/bin/perl                                                                                                

$number{"uno"} =      "1";
$number{"due"} =      "2";
$number{"tre"} =      "3";
$number{"quattro"} =  "4";
$number{"cinque"} =   "5";
$number{"sei"} =      "6";
$number{"sette"} =    "7";
$number{"otto"} =     "8";
$number{"nove"} =     "9";
$number{"zero"} =     "0";
$number{"punto"} =    ".";
$number{"più"} = " + ";
$number{"meno"} = " - ";
$number{"per"} = " * ";
$number{"diviso"} = " / ";
$number{"radice"} = "sqrt";
$number{"seno"} = "sin";
$number{"coseno"} = "cos";
$number{"di"} = "(";

$chiudi = 0;
for $arg (@ARGV) {
  $chiudi *= 2;
  $arg="\L$arg";
  if ($arg eq "di") {$chiudi=1;}
  if ($number{$arg} eq "") {
    $expression = join('', $expression, $arg);
  } else {
      $expression = join('', $expression, $number{$arg});
  }
  if ($chiudi == 2) {  $expression = join('', $expression, ")");}
}

$result = eval($expression);
print $result, "\n";

if ($result eq "") {
    open TTS, "tts -l it \"non lo so\"|";
    while (<TTS>){print;}
    close TTS;
} else {
    open TTS, "tts -l it \"fa $result\"|";
    while (<TTS>){print;}
    close TTS;
}

