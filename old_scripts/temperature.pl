#! /usr/bin/perl

$device = "/sys/bus/w1/devices/28-00000474e45f/w1_slave";

if ( -f $device ) {
    $lines[0] = "bla bla NO";
    $watchdog = 0;
    while ($lines[0] =~ /^.*NO$/) {
	open DEV, $device or die;
	@lines = <DEV>;
	close DEV;
	if ($watchdog == 10) {die "Thermometer not found or broken";}
	$watchdog++;
    }
    if ($lines[0] =~ /^.*YES$/) {
	if ($lines[1] =~ /^.*t=(\d+)$/) {
	    $temp = $1/1000;
	    print "$temp\n";
	}  else {print "not match temp!\n";}
    }  else {print "not match YES but $lines[0]!\n";}
} else {
    print "Device $device not found\n";
}
