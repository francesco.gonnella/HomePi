#! /usr/bin/perl
open  CONF, "/home/pi/caldaia.conf" or die;
@lines = <CONF>;
close CONF;

$hyst = 1;
@weekday = ("SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT");

($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year = $year + 1900;
$mon += 1;
#print "Formated time = $mday/$mon/$year $hour:$min:$sec $weekday[$wday]\n";
$on_off = 0;

for $line (@lines) {
  if ($line =~ /^(\w\w\w)\s+(\d+)(\.|\:)(\d+)\s+(\d+)(\.|\:)(\d+)(\s+(\d+))?\s*$/) {
      $day = "\U$1";
      $start_h = $2;
      $start_m = $4;
      $stop_h = $5;
      $stop_m = $7;
      $temperature = $8;
      if (($day eq $weekday[$wday]) and ($hour >= $start_h) and ($stop_h >= $hour)) {
	  if ( ($hour > $start_h) or ($min >= $start_m)) {
	      if ( ($stop_h > $hour) or ($stop_m >= $min) ) {
		  if ($temperature ne "" ) {
		      open TEMP, "/usr/games/temperature.pl |";
		      @temp = <TEMP>;
		      close GPIO;
		      if ($temp[0] =~ /^(\d*.\d+)$/) {
			  $t = $1;
			  if ($t < $temperature ) {
			      $on_off = 1;
			      last;
			  } elsif ($t > ($temperature + $hyst)) {
			      $on_off = 0;
			  }
		      } else {
			  print "Couldn't get the temperature, switching on!\n";
			  $on_off = 1;
			  last;
		      }
		      
		  } else {
		      $on_off = 1;
		      last;
		  }

	      }	      
	  }
      }
  } 
}







print "Caldaia must be $on_off \n";
open GPIO, "/usr/games/gpio14 $on_off |";
while (<GPIO>) {print;}
close GPIO;
