#! /usr/bin/perl

$dirname = "/home/pi/motion";
$donefile = "$dirname/done.txt";

opendir $dh, $dirname or die "Couldn't open dir '$dirname': $!";
@files = readdir $dh;
closedir $dh;

open  DONE, $donefile;
@done_file = <DONE>;
close DONE;

for my $don(@done_file) {
    chomp $don;
    $done{$don}=1;
#    print "*", $don , "* is done!\n" 
}

open DONE, ">>$donefile";

for my $file(@files) {
    if ($file =~ /.jpg$/) {
#	print "file: *", $file, "*\n";
	if ($done{$file} != 1) {

	    system("/usr/games/dropbox upload $dirname/$file RaspberryHome/$file &> /dev/null");
#	    print "dropbox upload $dirname/$file RaspberryHome/$file &> /dev/null\n";
	    print DONE "$file\n";
	}	
    }
}
close DONE;
