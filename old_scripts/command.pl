#! /usr/bin/perl                                                                                                
open  DIC, "/usr/games/command.dic";
@lines = <DIC>;
close DIC;

$i=0;
for $line (@lines) {
  $i++;
  if  (($line =~ /^\#.*$/) or ($line =~ /^\s*$/)) {
    #comment, spaces or empty line
  } elsif ($line =~ /^\s*(.+?)\s*\=\s*(.+?)$/) {
    $hush{$1} = $2;
    #print "key: \L$1, valore $2\n";
  } else {
    print "Error in dictionary file line $i: \"$line\"\n";
    exit; 
  }  
 
}

for $arg (@ARGV) {
  if ($hush{$arg} eq "") {
    print "\L$arg ";
  }else {
    print $hush{$arg};
  }

}
print "\n";
