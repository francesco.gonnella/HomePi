#!/usr/bin/perl
use Term::ANSIColor qw(:constants);
$Term::ANSIColor::AUTORESET = 1;

use File::Copy;

$dir = "/home/pi/qbittorrent/torrent";


opendir DIR, $dir;
@files = readdir DIR;
close DIR;

for my $file(@files) {

    $file_type = `/usr/bin/file $dir/$file`;
    print $file_type, "\n";
    if ($file =~ /^.*\.torrent$/) {
	print ".torrent!!!\n";
    } else {
	if ($file_type =~ /^$dir\/$file\:\sBitTorrent\sfile$/) {
	    move "$dir/$file", "/home/pi/qbittorrent/$file.torrent";
	    move "/home/pi/qbittorrent/$file.torrent", "$dir";
	} 
    }
    
    
}

