#! /usr/bin/perl

$temperature = 0.5; # Switch on if temperature is 1 degree or less
$hyst = 0.5; #Switch off if it's 1 more than the desired temperature

##### WATER AND AIR
open  CONF, "/home/pi/pond.conf" or die;
@lines = <CONF>;
close CONF;

@weekday = ("SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT");

($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year = $year + 1900;
$mon += 1;
#print "Formated time = $mday/$mon/$year $hour:$min:$sec $weekday[$wday]\n";
$on_off = 0;

for $line (@lines) {
  if ($line =~ /^(\w\w\w)\s+(\d+)(\.|\:)(\d+)\s+(\d+)(\.|\:)(\d+)(\s+(\d+))?\s*$/) {
      $day = "\U$1";
      $start_h = $2;
      $start_m = $4;
      $stop_h = $5;
      $stop_m = $7;
      if (($day eq $weekday[$wday]) and ($hour >= $start_h) and ($stop_h >= $hour)) {
	  if ( ($hour > $start_h) or ($min >= $start_m)) {
	      if ( ($stop_h > $hour) or ($stop_m >= $min) ) {
		  $on_off = 1;
		  last;
	      }	      
	  }
      }
  } 
}

print "Pond must be $on_off \n";
open GPIO, "/usr/bin/gpio  write 15 $on_off|" or die "Could not open gpio executable: $!";
while (<GPIO>) {print;}
close GPIO;
open GPIO, "/usr/bin/gpio  write 16 $on_off|" or die "Could not open gpio executable: $!";
while (<GPIO>) {print;}
close GPIO;





###### HEATING #####
# temperature and hysterisis are defined at the beginning

#get current status on or off
open GPIO, "/usr/bin/gpio read 5|" or die "Could not open gpio executable: $!";
@gpio = <GPIO>;
close GPIO;
if ($gpio[0] =~ /^.*([01]).*$/) {
    $on_off = $1;
    print "Heating is $on_off.\n";
} else {
    die "Could not parse gpio\n";
}

open TEMP, "/home/pi/HomePi/humidity/humidity |" or die "Could not open /home/pi/HomePi/humidity/humidity : $!\n";
@temp = <TEMP>;
close TEMP;
if ($temp[0] =~ /^(-?\d*\.?\d+).*$/) {
    $t = $1;
    if ($t <= $temperature ) {
	$on_off = 1;
    } elsif ($t >= ($temperature + $hyst)) {
	$on_off = 0;
    }
} else {
    print "Couldn't get the temperature, switching on!\n";
    $on_off = 1;
    last;
}

print "Desired on temperature is $temperature, off temperature is $temperature+$hyst, current temperature is $t, heating should be $on_off \n";


open GPIO, "/usr/bin/gpio mode 5 out|" or die "Could not set mode with gpio executable: $!";
while (<GPIO>) {print;}
close GPIO;
open GPIO, "/usr/bin/gpio write 5 $on_off|" or die "Could not write $on_off with gpio executable: $!";
while (<GPIO>) {print;}
close GPIO;
