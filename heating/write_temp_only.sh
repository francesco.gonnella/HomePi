#!/bin/bash
THIS_DIR="$(dirname "$0")"

if [[ `/usr/bin/raspi-gpio get 14` =~ "level=1" ]]; then
    ON_OFF=10
else
    ON_OFF=0
fi

/bin/echo `/bin/date "+%s"`  $ON_OFF `$THIS_DIR/temperature.pl all` >> /home/pi/temperature.all
head -1 /home/pi/temperature.all > /home/pi/temperature.txt
tail -n +2 /home/pi/temperature.all | tail -300 >>  /home/pi/temperature.txt


