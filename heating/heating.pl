#! /usr/bin/perl
open  CONF, "/home/pi/heating.conf" or die;
@lines = <CONF>;
close CONF;

$hyst = 1;
@weekday = ("SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT");

($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$year = $year + 1900;
$mon += 1;
#print "Formated time = $mday/$mon/$year $hour:$min:$sec $weekday[$wday]\n";
$on_off = "dl";

for $line (@lines) {
  if ($line =~ /^(\w\w\w)\s+(\d+)(\.|\:)(\d+)\s+(\d+)(\.|\:)(\d+)(\s+(\d+))?\s*$/) {
      $day = "\U$1";
      $start_h = $2;
      $start_m = $4;
      $stop_h = $5;
      $stop_m = $7;
      $temperature = $8;
      if (($day eq $weekday[$wday]) and ($hour >= $start_h) and ($stop_h >= $hour)) {
	  if ( ($hour > $start_h) or ($min >= $start_m)) {
	      if ( ($stop_h > $hour) or ($stop_m >= $min) ) {
		  if ($temperature ne "" ) {
		      open TEMP, "/usr/local/bin/temperature.pl |" or die "Could not open /usr/local/bin/temperature.pl: $!\n";
		      @temp = <TEMP>;
		      close TEMP;
		      if ($temp[0] =~ /^(\d*\.?\d+).*$/) {
			  $t = $1;
			  if ($t < $temperature ) {
			      $on_off = "dh";
			      last;
			  } elsif ($t > ($temperature + $hyst)) {
			      $on_off = "dl";
			  }
		      } else {
			  print "Couldn't get the temperature, switching on!\n";
			  $on_off = "dh";
			  last;
		      }
		      
		  } else {
		      $on_off = "dh";
		      last;
		  }

	      }	      
	  }
      }
  } 
}

print "Caldaia must be $on_off, checking override... \n";
$override_file = "/home/pi/overrides/override";

if ($on_off eq "dh") {
    if (-e "$override_file\_on") {
	print "Deleting $override_file\_on...\n" ;
	unlink("$override_file\_on");
    }
    if (-e "$override_file\_off") {
	print "Off override found, caldaia will be switched off\n";
	$on_off = "dl";
    }
} else {
    if (-e "$override_file\_off") {
	print "Deleting $override_file\_off...\n" ;
	unlink("$override_file\_off");
    }
    if (-e "$override_file\_on") {
	print "On override found, caldaia will be switched on\n";
	$on_off = "dh";
    }
}

open GPIO, "/usr/bin/raspi-gpio set 14 op |" or die "Could not open gpio executable: $!";
while (<GPIO>) {print;}
close GPIO;

open GPIO, "/usr/bin/raspi-gpio set 14 $on_off|" or die "Could not open gpio executable: $!";
while (<GPIO>) {print;}
close GPIO;
