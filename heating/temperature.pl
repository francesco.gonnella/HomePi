#! /usr/bin/perl
$n = $#ARGV == -1?0:$ARGV[0];

$dirname = '/sys/bus/w1/devices/';
opendir(DIR, $dirname) or die "Could not open $dirname\n";
$i = 0;
while ($filename = readdir(DIR)) {
    $dev = "$dirname/$filename/w1_slave";
    if (-e $dev) {
	if (!($n+0 eq $n) || $i == $n) {
	    push @devices, $dev;
	}
    $i++;
    }
}
closedir(DIR);

    
foreach  $device (@devices) {
    if ( -f $device ) {
	$lines[0] = "bla bla NO";
	$watchdog = 0;
	while ($lines[0] =~ /^.*NO$/) {
	    open DEV, $device or die;
	    @lines = <DEV>;
	    close DEV;
	    if ($watchdog == 10) {die "Thermometer not found or broken";}
	    $watchdog++;
	}
	if ($lines[0] =~ /^.*YES$/) {
	    if ($lines[1] =~ /^.*t=(-?\d+)$/) {
		$temp = $1/1000;
		print "$temp ";
	    }  else {print "not match temp!\n";}
	}  else {print "not match YES but $lines[0]!\n";}
    } else {
	print "Device $device not found\n";
    }
}
print "\n";
