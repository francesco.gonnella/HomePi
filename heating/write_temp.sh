#!/bin/bash
THIS_DIR="$(dirname "$0")"

HUM=`$THIS_DIR/../humidity/humidity | awk '{print $2}'`
TEM=`$THIS_DIR/../humidity/humidity | awk '{print $1}'`
ON_OFF=`/usr/bin/gpio read 5`
/bin/echo  `/bin/date "+%s"` $TEM `$THIS_DIR/temperature.pl all` >> /home/pi/temperature.all
/bin/echo  `/bin/date "+%s"` $HUM ${ON_OFF}00  >> /home/pi/humidity.all
head -1 /home/pi/temperature.all > /home/pi/temperature.txt
head -1 /home/pi/humidity.all > /home/pi/humidity.txt
tail -n +2 /home/pi/temperature.all | tail -300 >>  /home/pi/temperature.txt
tail -n +2 /home/pi/humidity.all | tail -300 >>  /home/pi/humidity.txt
