<!doctype html>
<html lang="en">

<?php

$fn = "/home/pi/pond.conf";



if (isset($_POST['content']))

{

    $content = stripslashes($_POST['content']);

    $fp = fopen($fn,"w") or die ("Error opening file in write mode!");

    fputs($fp,$content);

    fclose($fp) or die ("Error closing file!");

}


if ($_GET['water'] === "on" ) {
  exec("/usr/bin/gpio  mode 15 out");
  exec("/usr/bin/gpio  write 15 1");
}

if ($_GET['water'] === "off" ) {
  exec("/usr/bin/gpio  mode 15 out");
  exec("/usr/bin/gpio  write 15 0");
}

if ($_GET['air'] === "on" ) {
  exec("/usr/bin/gpio  mode 16 out");
  exec("/usr/bin/gpio  write 16 1");
}

if ($_GET['air'] === "off" ) {
  exec("/usr/bin/gpio  mode 16 out");
  exec("/usr/bin/gpio  write 16 0");
}

if ($_GET['heat'] === "on" ) {
  exec("/usr/bin/gpio  mode 5 out");
  exec("/usr/bin/gpio  write 5 1");
}

if ($_GET['heat'] === "off" ) {
  exec("/usr/bin/gpio  mode 5 out");
  exec("/usr/bin/gpio  write 5 0");
}


$water = exec('/usr/bin/gpio read 15');
$air   = exec('/usr/bin/gpio read 16');
$heat  = exec('/usr/bin/gpio read 5');

exec("/usr/local/bin/pond.pl");
?>

<!-- <script src='delayedLoading.js' type="text/javascript"></script> -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
<title> Pond </title>
<LINK REL="SHORTCUT ICON" HREF="rpi.ico" />
 <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>


<nav class="navbar navbar-expand-sm navbar-light bg-light fixed-top">

<div>
  <span>
    W:
  <a href="?water=on" class="btn btn-outline-primary btn-sm active" role="button" aria-pressed="true">ON</a>
    <a href="?water=off" class="btn btn-outline-secondary btn-sm active" role="button" aria-pressed="true">OFF</a>
    <?php if ($water == "1"): ?> 
    <span class="badge badge-pill badge-success">I</span>
    <?php else: ?>
    <span class="badge badge-pill badge-danger">O</span>  
    <?php endif ?>

  </span>
  </div>

<div>
  <span>
    A:
    <a href="?air=on" class="btn btn-outline-primary btn-sm active" role="button" aria-pressed="true">ON</a>
    <a href="?air=off" class="btn btn-outline-secondary btn-sm active" role="button" aria-pressed="true">OFF</a>
    <?php if ($air == "1"): ?> 
    <span class="badge badge-pill badge-success">I</span>
    <?php else: ?>
    <span class="badge badge-pill badge-danger">O</span>  
    <?php endif ?>
  </span>
</div>

<div>
  <span>
    H:
    <a href="?heat=on" class="btn btn-outline-primary btn-sm active" role="button" aria-pressed="true">ON</a>
    <a href="?heat=off" class="btn btn-outline-secondary btn-sm active" role="button" aria-pressed="true">OFF</a>
    <?php if ($heat == "1"): ?> 
    <span class="badge badge-pill badge-success">I</span>
    <?php else: ?>
    <span class="badge badge-pill badge-danger">O</span>  
    <?php endif ?>
  </span>
</div>
</nav>


<div class="container">
<p> <h3> Pond configuration </h3> </p>
<p> Syntax: DAY hh:mm hh:mm <temp> </p>
<p> Example:  WED 17:00 23:55 </p>
</div>

<div class="container">
<form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="post">

    <textarea class="form-control" rows="25" name="content"><?php readfile($fn); ?></textarea>
    <input type="submit" value="Save" class="btn btn-primary" > 

</form>
</div>

<div class="container">
<canvas id="scatterChart"></canvas>
</div>

<div class="container">
<canvas id="scatterChart2"></canvas>
</div>

<div id="chartContainer" style="width:100%; height:300px;"></div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script  type="text/javascript"  src="js/jquery.min.js"></script>
    <script type="text/javascript"  src="js/popper.min.js" ></script>
    <script src="js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/moment.js"></script>
    <script type="text/javascript" src="js/mdb.min.js"></script>


<script type="text/javascript">
  window.onload = function() {
        function getDataPointsFromCSV(csv) {
            var dataPoints = {};
	    var csvLines = points = [];
            csvLines = csv.split(/[\r?\n|\r|\n]+/);         
            if (csvLines[0].length > 1) {
		//Titles
		dataPoints["SENSORS"]  = [];
                names = csvLines[0].split(/\s+/);
		n_sensors = names.length-1;
		for (var j = 1; j <= n_sensors; j++) {
		    console.log(names[j]);
		    dataPoints["SENSORS"].push(names[j]);
		    dataPoints[names[j]] = [];		    
		}
		console.log(dataPoints);		
	    }
            for (var i = 1; i < csvLines.length; i++)
                if (csvLines[i].length > 1) {
                    points = csvLines[i].split(/\s+/);
		    for (var k = 0; k < n_sensors; k++) {
			dataPoints[dataPoints["SENSORS"][k]].push({ 
                            x: new Date(parseFloat(points[0]*1000)), 
                            y: parseFloat(points[k+1])
			});
		    }
                }
            return dataPoints;
        }
	
	$.get("../temperature.txt", function(data) {

	    var ctxSc = document.getElementById('scatterChart').getContext('2d');

	    var DataPoints = getDataPointsFromCSV(data);
	    var scatterData = { datasets: []};
	    var colours = ["rgba(125,0,0, .5)", "rgba(10,10,125, .5)", "rgba(20,125,25, .5)", "rgba(10,50,125, .5)", "rgba(10,50,125, .5)"];

	    
	  	    for (i=0; i<DataPoints["SENSORS"].length; i++) {
		scatterData["datasets"].push( {
		    borderColor: 'rgba(0,0,0, .2)',
		    backgroundColor: colours[i],
		    label: DataPoints["SENSORS"][i],
		    data: DataPoints[DataPoints["SENSORS"][i]]
		})
	    }
	    var config1 = new Chart.Scatter(ctxSc, {
		data: scatterData,
		options: {
		    title: {
			display: true,
			text: 'Temperature Chart'
		    },
		    scales: {
			xAxes: [{
			    type: 'time',
			    position: 'bottom',
			    scaleLabel: {
				labelString: 'Date',
				display: true,
			    }
			}],
			yAxes: [{
			    type: 'linear',
			    ticks: {
				userCallback: function (tick) {
				    return tick.toString() + ' °C';
				}
			    },
			    scaleLabel: {
				labelString: 'Temperature',
				display: true
			    }
			}]
		    }
		}
	    });
	});

	$.get("../humidity.txt", function(data) {

	    var ctxSc2 = document.getElementById('scatterChart2').getContext('2d');

	    var DataPoints = getDataPointsFromCSV(data);
	    var scatterData = { datasets: []};
	    var colours = ["rgba(0,200,0, .5)", "rgba(10,10,125, .5)", "rgba(20,125,25, .5)", "rgba(10,50,125, .5)", "rgba(10,50,125, .5)"];

	    
	    for (i=0; i<DataPoints["SENSORS"].length; i++) {
		scatterData["datasets"].push( {
		    borderColor: 'rgba(0,0,0, .2)',
		    backgroundColor: colours[i],
		    label: DataPoints["SENSORS"][i],
		    data: DataPoints[DataPoints["SENSORS"][i]]
		})
	    }
	    var config1 = new Chart.Scatter(ctxSc2, {
		data: scatterData,
		options: {
		    title: {
			display: true,
			text: 'Humidity Chart'
		    },
		    scales: {
			xAxes: [{
			    type: 'time',
			    position: 'bottom',
			    scaleLabel: {
				labelString: 'Date',
				display: true,
			    }
			}],
			yAxes: [{
			    type: 'linear',
			    ticks: {
				userCallback: function (tick) {
				    return tick.toString() + ' %';
				}
			    },
			    scaleLabel: {
				labelString: 'Humidity',
				display: true
			    }
			}]
		    }
		}
	    });
	});

    }
</script>

<?php
$handle = fopen("/sys/bus/w1/devices/w1_bus_master1/w1_master_slaves", "r");
if ($handle) {
    while (($sensors = fgets($handle)) !== false) {
           $sensor = "/sys/bus/w1/devices/".trim($sensors)."/w1_slave";
           $sensorhandle = fopen($sensor, "r");
             if ($sensorhandle) {
                 $thermometerReading = fread($sensorhandle, filesize($sensor));
                 fclose($sensorhandle);
                 // We want the value after the t= on the 2nd line
                 preg_match("/t=(.+)/", preg_split("/\n/", $thermometerReading)[1], $matches);
                 $celsius = $matches[1] / 1000;
                 print "Sensor ID#: $sensors = $celsius &deg;C<br>";
                 $sensors++;
             } else {
                print "No temperature read!";
             }
    }
    fclose($handle);



} else {
    print "No sensors found!";
}
?>


<p></p>

</body>
</html>

